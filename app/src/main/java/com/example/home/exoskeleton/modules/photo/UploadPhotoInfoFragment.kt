package com.example.home.exoskeleton.modules.photo

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.home.exoskeleton.R
import kotlinx.android.synthetic.main.fragment_upload_info.*
import pro.horovodovodo4ka.bones.Phalanx
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.ScreenInterface
import pro.horovodovodo4ka.bones.ui.delegates.Page

class UploadPhotoInfoBone(val image : String) : Phalanx(){
    override val seed = { UploadPhotoInfoFragment() }
}

class UploadPhotoInfoFragment : Fragment(), ScreenInterface<UploadPhotoInfoBone> by Page(), BonePersisterInterface<UploadPhotoInfoBone> {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.fragment_upload_info, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        textView3.text = bone.image
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<android.support.v4.app.Fragment>.onSaveInstanceState(outState)
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<android.support.v4.app.Fragment>.onCreate(savedInstanceState)
    }
}