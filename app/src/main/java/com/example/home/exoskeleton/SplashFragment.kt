package com.example.home.exoskeleton

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.home.exoskeleton.modules.cardlist.fragments.CardListBone
import com.example.home.exoskeleton.modules.photo.UploadPhotoBone
import com.example.home.exoskeleton.modules.profile.fragments.ProfileNavigationStack
import com.example.home.exoskeleton.navigation.NavigationStackBone
import com.example.home.exoskeleton.navigation.RootSpineBone
import com.example.home.exoskeleton.navigation.TabBarBone
import com.example.home.exoskeleton.network.RestService
import com.example.home.exoskeleton.network.res.PhotoCardRes
import com.example.home.exoskeleton.services.RealmService
import com.example.home.exoskeleton.storage.PhotoCard
import com.github.salomonbrys.kodein.conf.KodeinGlobalAware
import com.github.salomonbrys.kodein.instance
import kotlinx.coroutines.experimental.runBlocking
import pro.horovodovodo4ka.bones.Finger
import pro.horovodovodo4ka.bones.Phalanx
import pro.horovodovodo4ka.bones.extensions.closest
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.ScreenInterface
import pro.horovodovodo4ka.bones.ui.delegates.Page

class SplashBone : Phalanx(), KodeinGlobalAware {
    val restService : RestService = instance()
    val realmService : RealmService = instance()

    init {
        var list : List<PhotoCardRes> = emptyList()
        runBlocking {
            list = restService.getPhotoCardList("Thu Jan 1 1970 00:00:00 GMT+0000 (UTC)",150,0).await()

        }
        realmService.cardList = list.map { PhotoCard(it) }
    }

    fun showRootView() {
        val bot = closest<Finger>()?.phalanxes?.first()
        val dlg = RootSpineBone(TabBarBone(NavigationStackBone(CardListBone()),
                ProfileNavigationStack(),
                NavigationStackBone(UploadPhotoBone())))
        closest<Finger>()?.replace(bot, dlg)
    }

    override val seed = { SplashFragment() }
}

class SplashFragment : Fragment(), ScreenInterface<SplashBone> by Page(), BonePersisterInterface<SplashBone> {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_splash, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Handler().postDelayed({
            bone.showRootView()
        },3000)
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<Fragment>.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<Fragment>.onCreate(savedInstanceState)
    }
}