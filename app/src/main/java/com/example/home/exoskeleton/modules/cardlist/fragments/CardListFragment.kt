package com.example.home.exoskeleton.modules.cardlist.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.home.exoskeleton.R
import com.example.home.exoskeleton.modules.cardlist.adapters.CardListAdapter
import com.example.home.exoskeleton.modules.cardlist.datasources.CardListDataSource
import com.example.home.exoskeleton.modules.profile.fragments.SignInBone
import com.example.home.exoskeleton.modules.profile.fragments.SignUpBone
import com.example.home.exoskeleton.navigation.MenuItemHolder
import com.example.home.exoskeleton.navigation.NavigationStackBone
import com.example.home.exoskeleton.navigation.NavigationStackPresentable
import com.example.home.exoskeleton.services.UserService
import com.example.home.exoskeleton.services.UserServiceDelegate
import com.example.home.exoskeleton.storage.PhotoCard
import com.github.salomonbrys.kodein.conf.KodeinGlobalAware
import com.github.salomonbrys.kodein.instance
import kotlinx.android.synthetic.main.fragment_card_list.*
import pro.horovodovodo4ka.bones.BoneSibling
import pro.horovodovodo4ka.bones.Finger
import pro.horovodovodo4ka.bones.Phalanx
import pro.horovodovodo4ka.bones.extensions.closest
import pro.horovodovodo4ka.bones.extensions.present
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.delegates.Page

class CardListBone : Phalanx(), NavigationStackPresentable, KodeinGlobalAware, UserServiceDelegate {

    private val userService: UserService = instance()

    init {
        userService.delegate += this
    }

    override var menuItem: MenuItemHolder? = MenuItemHolder().also {
        it.menuRes = userService.getUserToken()?.let { R.menu.maint_toolbar_exit_menu } ?: R.menu.main_toolbar_menu
        it.menuItemClickListener = { item ->
            when (item.itemId) {
                R.id.toolbar_search -> {
                    closest<Finger>()?.push(SearchFilterBone())
                }
                R.id.toolbar_signUp -> {
                    present(SignUpBone())
                }
                R.id.toolbar_signIn -> {
                    present(SignInBone())
                }
                R.id.toolbar_exit -> {
                    userService.logOut()
                }

            }
            true
        }
    }

    override fun userWasChanged() {
        when (userService.getUserToken()) {
            null -> {
                menuItem = MenuItemHolder().apply {
                    menuRes = R.menu.main_toolbar_menu
                    menuItemClickListener = { item ->
                        when (item.itemId) {
                            R.id.toolbar_search -> {
                                closest<Finger>()?.push(SearchFilterBone())
                            }
                            R.id.toolbar_signUp -> {
                                present(SignUpBone())
                            }
                            R.id.toolbar_signIn -> {
                                present(SignInBone())
                            }

                        }
                        true
                    }
                }
            }
            else -> {
                menuItem = MenuItemHolder().apply {
                    menuRes = R.menu.maint_toolbar_exit_menu
                    menuItemClickListener = { item ->
                        when (item.itemId) {
                            R.id.toolbar_exit -> {
                                userService.logOut()
                            }
                        }
                        true
                    }
                }
            }
        }
        closest<NavigationStackBone>()?.updateMenu()
    }

    override val fragmentTitle = "ЭКЗОСКЕЛЕТ"
    override val seed = { CardListFragment() }

    fun showPhotoInfoScrewen(card: PhotoCard) {
        closest<Finger>()?.push(PhotoCardInfoBone(card))
    }
}

class CardListFragment : Fragment(),
    BoneSibling<CardListBone> by Page(),
    BonePersisterInterface<CardListBone> {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_card_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(cardListRecycler) {
            adapter = CardListAdapter(CardListDataSource()) {
                bone.showPhotoInfoScrewen(it)
            }
            layoutManager = GridLayoutManager(context, 2)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<android.support.v4.app.Fragment>.onSaveInstanceState(outState)
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<android.support.v4.app.Fragment>.onCreate(savedInstanceState)
    }
}