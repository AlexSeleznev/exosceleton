package com.example.home.exoskeleton.modules.profile.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import com.example.home.exoskeleton.R
import com.example.home.exoskeleton.modules.profile.AlbumDataSource
import com.example.home.exoskeleton.storage.Album
import kotlinx.android.synthetic.main.profile_album.view.*

class ProfileAlbumAdapter(dataSource: AlbumDataSource) : RecyclerView.Adapter<ProfileAlbumAdapter.ProfileAlbumViewHolder>() {

    private val albums : List<Album> = dataSource.albums
    lateinit var listener : (Album)-> Unit


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileAlbumViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ProfileAlbumViewHolder(inflater.inflate(R.layout.profile_album, parent, false))
    }

    override fun getItemCount(): Int  = albums.size


    override fun onBindViewHolder(holder: ProfileAlbumViewHolder, position: Int) {
        val albumInfo = albums[position]
        holder.albumTitle.text = albumInfo.title
        holder.albumCardCount.text = albumInfo.photoCardList.size.toString()
        holder.albumFavorite.text = albumInfo.favorites.toString()
        holder.albumViews.text = albumInfo.views.toString()
    }


    inner class ProfileAlbumViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val albumTitle: TextView = itemView.albumTitle
        val albumCardCount: TextView = itemView.albumCardCount
        val albumViews: TextView = itemView.viewsCount
        val albumFavorite: TextView = itemView.favoriteCount
        val container : FrameLayout = itemView.albumContainer

        init {
            container.setOnClickListener {
                listener.invoke(albums[adapterPosition])
            }
        }

    }
}