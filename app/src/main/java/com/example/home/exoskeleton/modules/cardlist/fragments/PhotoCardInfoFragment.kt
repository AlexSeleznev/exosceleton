package com.example.home.exoskeleton.modules.cardlist.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import com.example.home.exoskeleton.navigation.MenuItemHolder
import com.example.home.exoskeleton.navigation.NavigationStackPresentable
import com.example.home.exoskeleton.storage.PhotoCard
import pro.horovodovodo4ka.bones.BoneSibling
import pro.horovodovodo4ka.bones.Phalanx
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.delegates.Page

class PhotoCardInfoBone(photoCard: PhotoCard) : Phalanx(), NavigationStackPresentable {
    override var menuItem: MenuItemHolder? = null
    override val fragmentTitle = photoCard.title
    override val seed = { PhotoCardInfoFragment() }
}

class PhotoCardInfoFragment : Fragment(),
    BoneSibling<PhotoCardInfoBone> by Page(),
    BonePersisterInterface<PhotoCardInfoBone> {

    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<android.support.v4.app.Fragment>.onSaveInstanceState(outState)
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<android.support.v4.app.Fragment>.onCreate(savedInstanceState)
    }

}