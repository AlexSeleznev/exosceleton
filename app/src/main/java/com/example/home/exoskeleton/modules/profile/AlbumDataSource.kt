package com.example.home.exoskeleton.modules.profile

import com.example.home.exoskeleton.services.RealmService
import com.example.home.exoskeleton.storage.Album
import com.github.salomonbrys.kodein.conf.KodeinGlobalAware
import com.github.salomonbrys.kodein.instance

class AlbumDataSource : KodeinGlobalAware {
    private val realmService : RealmService = instance()

    val albums : List<Album>
    get() {
        return realmService.userAlbums
    }
}