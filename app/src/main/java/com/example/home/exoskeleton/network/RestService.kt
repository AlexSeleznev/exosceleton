package com.example.home.exoskeleton.network

import com.example.home.exoskeleton.network.req.AddAlbumReq
import com.example.home.exoskeleton.network.req.UserSignInReq
import com.example.home.exoskeleton.network.req.UserSignUpReq
import com.example.home.exoskeleton.network.res.PhotoCardRes
import com.example.home.exoskeleton.network.res.UploadPhotoRes
import com.example.home.exoskeleton.network.res.UserAlbumRes
import com.example.home.exoskeleton.network.res.UserRes
import kotlinx.coroutines.experimental.Deferred
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query


interface RestService {
    @GET("photocard/list")
    fun getPhotoCardList(@Header("If-Modified-Since") lastEntityUpdate: String, @Query("limit") limit: Int, @Query("offset") offset: Int): Deferred<List<PhotoCardRes>>

    @POST("photocard/{photoCardId}/view")
    fun addView(@Path("photoCardId") photoCardId: String): Deferred<String>

    @POST("user/signUp")
    fun sigUpUser(@Body user: UserSignUpReq): Deferred<UserRes>

    @POST("user/signIn")
    fun signInUser(@Body user: UserSignInReq): Deferred<Response<UserRes>>

    @POST("user/{userId}/album")
    fun createAlbum(@Path("userId") userId: String, @Header("Authorization") userToken: String, @Body album: AddAlbumReq): Deferred<UserAlbumRes>

    @Multipart
    @POST("user/{userId}/image/upload")
    fun uploadPhoto(@Path("userId") userId : String, @Part file : MultipartBody.Part, @Header("Authorization") userToken : String) : Deferred<UploadPhotoRes>

}
