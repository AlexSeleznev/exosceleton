package com.example.home.exoskeleton.network.res

data class UserRes(val id : String,
                   val name: String,
                   val login: String,
                   var albums: List<UserAlbumRes>,
                   val token: String,
                   val avatar : String,
                   val photocardCount : Int,
                   val albumCount : Int)
