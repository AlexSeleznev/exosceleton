package com.example.home.exoskeleton.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.home.exoskeleton.Broadcaster
import com.example.home.exoskeleton.R
import com.example.home.exoskeleton.SplashBone
import com.example.home.exoskeleton.modules.photo.ResultFromActivity
import com.example.home.exoskeleton.notify
import pro.horovodovodo4ka.bones.Bone
import pro.horovodovodo4ka.bones.Finger
import pro.horovodovodo4ka.bones.extensions.glueWith
import pro.horovodovodo4ka.bones.extensions.processBackPress
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.statesstore.EmergencyPersister
import pro.horovodovodo4ka.bones.statesstore.EmergencyPersisterInterface
import pro.horovodovodo4ka.bones.ui.FingerNavigatorInterface
import pro.horovodovodo4ka.bones.ui.delegates.FingerNavigator
import pro.horovodovodo4ka.bones.ui.helpers.ActivityAppRestartCleaner

class RootFinger(root: Bone) : Finger(root) {
    init {
        persistSibling = true
    }
}

class RootActivity : AppCompatActivity(), FingerNavigatorInterface<RootFinger> by FingerNavigator(android.R.id.content), BonePersisterInterface<RootFinger>,
    EmergencyPersisterInterface<RootActivity> by EmergencyPersister(), ActivityAppRestartCleaner {


    init {
        managerProvider = ::getSupportFragmentManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super<AppCompatActivity>.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
        if (!emergencyLoad(savedInstanceState, this)) {

            super<ActivityAppRestartCleaner>.onCreate(savedInstanceState)

            bone = RootFinger(SplashBone())

            glueWith(bone)
            bone.isActive = true

            supportFragmentManager
                .beginTransaction()
                .replace(android.R.id.content, bone.phalanxes.first().sibling as Fragment)
                .commit()
        } else {
            glueWith(bone)
        }
    }

    override fun onBackPressed() {
        if (bone.processBackPress()) finish()
    }

    override fun onResume() {
        super.onResume()

        emergencyRemovePin()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Broadcaster.notify<ResultFromActivity> { resultFromActivity(requestCode, resultCode, data) }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super<AppCompatActivity>.onSaveInstanceState(outState)
        emergencyPin(outState)
    }

    @SuppressLint("MissingSuperCall")
    override fun onDestroy() {
        super.onDestroy()

        with(bone) {
            sibling = null
            emergencySave {
                it.bone = this
            }
        }
    }

}
