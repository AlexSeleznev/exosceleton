package com.example.home.exoskeleton

import java.lang.ref.WeakReference
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

object Broadcaster {

    fun <T: Any> register(clz: KClass<T>, receiver: T) {
        registry[clz] = registry[clz] ?: mutableListOf()
        // skip if exists
        registry[clz]?.find { it.get() === receiver }?.run { return }
        registry[clz]!! += receiver.weak()
    }

    fun <T: Any> notify(clz: KClass<T>, block: T.() -> Unit) {
        cleanup()
        @Suppress("UNCHECKED_CAST")
        registry[clz]?.forEach { block(it.get() as T) }
    }

    fun <T: Any> unregister(clz: KClass<T>, receiver: T) {
        registry[clz]?.retainAll { it.get() !== receiver }
    }

    private fun cleanup() {
        registry.forEach { it.value.retainAll { it.get() != null } }
    }

    private val registry = mutableMapOf<KClass<*>, MutableList<WeakReference<*>>>()

}

inline fun <reified T: Any> Broadcaster.register(receiver: T) {
    register(T::class, receiver)
}

inline fun <reified T: Any> Broadcaster.unregister(receiver: T) {
    unregister(T::class, receiver)
}

inline fun <reified T: Any>Broadcaster.notify(noinline block: T.() -> Unit) {
    notify(T::class, block)
}

class WeakRef<T>(obj: T? = null) : ReadWriteProperty<Any?, T?> {

    private var wref: WeakReference<T>?

    init {
        this.wref = obj?.let { WeakReference(it) }
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): T? {
        return wref?.get()
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T?) {
        wref = value?.let { WeakReference(it) }
    }
}

fun <T> weak(obj: T? = null) = WeakRef(obj)

fun <T> T.weak() = WeakReference(this)