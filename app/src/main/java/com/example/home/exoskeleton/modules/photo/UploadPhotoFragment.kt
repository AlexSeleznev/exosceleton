package com.example.home.exoskeleton.modules.photo

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.home.exoskeleton.Broadcaster
import com.example.home.exoskeleton.R
import com.example.home.exoskeleton.network.RestService
import com.example.home.exoskeleton.register
import com.example.home.exoskeleton.services.RealmService
import com.github.salomonbrys.kodein.conf.KodeinGlobalAware
import com.github.salomonbrys.kodein.instance
import kotlinx.android.synthetic.main.fragment_add_card.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pro.horovodovodo4ka.bones.Finger
import pro.horovodovodo4ka.bones.Phalanx
import pro.horovodovodo4ka.bones.extensions.closest
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.ScreenInterface
import pro.horovodovodo4ka.bones.ui.delegates.Page
import java.io.InputStream
import java.util.UUID

class UploadPhotoBone : Phalanx(), KodeinGlobalAware {
    private val restService: RestService = instance()
    private val realmService: RealmService = instance()

    fun uploadPhoto(toString: InputStream?) {
        val sendFile = RequestBody.create(MediaType.parse("multipart/form-data"), toString?.readBytes())
        val body = MultipartBody.Part.createFormData("image", UUID.randomUUID().toString(), sendFile)
        val user = realmService.getUserInfo()
        launch(UI) {
            try {
               val image =  restService.uploadPhoto(user?.id!!, body, user.token).await().image
                closest<Finger>()?.push(UploadPhotoInfoBone(image))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override val seed = { UploadPhotoFragment() }
}

class UploadPhotoFragment : Fragment(), ScreenInterface<UploadPhotoBone> by Page(), BonePersisterInterface<UploadPhotoBone>, ResultFromActivity {

    override fun resultFromActivity(requestCode: Int, resultCode: Int, data: Intent?) {
        val i = activity?.contentResolver?.openInputStream(data?.data)
        bone.uploadPhoto(i)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_add_card, container, false)

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uploadButton.setOnClickListener {
            val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
            activity?.requestPermissions(permissions, 102)
            uploadPhoto()
        }
    }

    override fun onResume() {
        super.onResume()
        Broadcaster.register<ResultFromActivity>(this)
    }

    private fun uploadPhoto() {
        val intent = Intent().apply {
            type = "image/*"
            action = Intent.ACTION_OPEN_DOCUMENT
            addCategory(Intent.CATEGORY_OPENABLE)
        }
        activity?.startActivityForResult(intent, 101)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<android.support.v4.app.Fragment>.onSaveInstanceState(outState)
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<android.support.v4.app.Fragment>.onCreate(savedInstanceState)
    }
}

interface ResultFromActivity {
    fun resultFromActivity(requestCode: Int, resultCode: Int, data: Intent?)
}