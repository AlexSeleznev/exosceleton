package com.example.home.exoskeleton.network.req

class AddAlbumReq(val title: String, val description: String)