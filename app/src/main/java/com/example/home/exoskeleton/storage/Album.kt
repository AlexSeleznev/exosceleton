package com.example.home.exoskeleton.storage

import com.example.home.exoskeleton.network.res.UserAlbumRes
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Album() : RealmObject() {
    @PrimaryKey
    var id = ""
    var owner = ""
    var title = ""
    var preview : String? = ""
    var description = ""
    var views = 0
    var favorites = 0
    var photoCardList = RealmList<PhotoCard>()

    constructor(userAlbum : UserAlbumRes) : this() {
        this.id = userAlbum.id
        this.owner = userAlbum.owner
        this.title = userAlbum.title
        this.preview = userAlbum.preview
        this.description = userAlbum.description
        this.views = userAlbum.views
        this.favorites = userAlbum.favorits
        this.photoCardList = RealmList(*userAlbum.photocards.map {PhotoCard(it)}.toTypedArray())
    }
}