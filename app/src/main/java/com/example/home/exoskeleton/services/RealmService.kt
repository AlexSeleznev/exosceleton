package com.example.home.exoskeleton.services

import com.example.home.exoskeleton.network.res.UserAlbumRes
import com.example.home.exoskeleton.network.res.UserRes
import com.example.home.exoskeleton.storage.Album
import com.example.home.exoskeleton.storage.PhotoCard
import com.example.home.exoskeleton.storage.User
import io.realm.Realm

class RealmService {

    private val realm: Realm by lazy {
        Realm.getDefaultInstance()
    }

    fun getUserInfo(): User? {
        return realm.where(User::class.java).findFirst()
    }

    val userAlbums : List<Album>
    get() {
       return realm.where(Album::class.java).equalTo("owner", getUserInfo()?.id).findAll()
    }

    fun saveUserInfo(user:UserRes){
        val user = User(user)
        realm.executeTransaction{it.insertOrUpdate(user)}
    }

    fun saveAlbumToRealm(it: UserAlbumRes, userID: String?, userToken: String?) {
        val user = realm.where(User::class.java).equalTo("token", userToken).findFirst()

        val albumRealm = Album(it)

        realm.executeTransaction { realm ->  user?.albums?.add(albumRealm) }
    }

    var cardList : List<PhotoCard> = emptyList()
    get() {
        return realm.where(PhotoCard::class.java).findAll()
    }
    set(value) {
        field = value
        realm.executeTransaction {
            it.insertOrUpdate(value)
        }

    }

    fun logout() {
        realm.executeTransaction{ realm -> realm.delete(User::class.java)}
    }

}