package com.example.home.exoskeleton.network.res

class UserAlbumRes(val owner: String,
                   val title: String,
                   val active: Boolean,
                   val isFavorite : Boolean,
                   val description: String,
                   val preview: String,
                   val photocards: List<PhotoCardRes>,
                   val id: String,
                   val views: Int,
                   val favorits: Int)