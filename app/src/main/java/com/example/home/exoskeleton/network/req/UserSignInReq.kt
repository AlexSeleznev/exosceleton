package com.example.home.exoskeleton.network.req

data class UserSignInReq(val email: String,
                         val password: String)