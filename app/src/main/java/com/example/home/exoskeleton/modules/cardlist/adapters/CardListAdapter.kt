package com.example.home.exoskeleton.modules.cardlist.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.home.exoskeleton.R
import com.example.home.exoskeleton.modules.cardlist.datasources.CardListDataSource
import com.example.home.exoskeleton.network.res.PhotoCardRes
import com.example.home.exoskeleton.storage.PhotoCard
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_photo_card.view.*

class CardListAdapter(dataSource : CardListDataSource, val callBack : (PhotoCard)->Unit) : RecyclerView.Adapter<CardListAdapter.CardListViewHolder>() {


    private val cardLisrt : List<PhotoCard> = dataSource.cardList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_photo_card, parent, false)
        return CardListViewHolder(view)
    }

    override fun getItemCount(): Int  = cardLisrt.size

    override fun onBindViewHolder(holder: CardListViewHolder, position: Int) {
       holder.bind(cardLisrt[position])
    }


    inner class CardListViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        fun bind(photoCard : PhotoCard){
            Picasso.with(itemView.context).load(photoCard.photo).into(itemView.card_image)
            itemView.favorite_count_txt.text = photoCard.favorites.toString()
            itemView.views_count_txt.text = photoCard.views.toString()
            itemView.card_image.setOnClickListener { callBack.invoke(photoCard) }
        }
    }
}