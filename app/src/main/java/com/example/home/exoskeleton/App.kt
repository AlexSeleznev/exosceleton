package com.example.home.exoskeleton

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.example.home.exoskeleton.network.RestService
import com.example.home.exoskeleton.services.RealmService
import com.example.home.exoskeleton.services.UserService
import com.github.salomonbrys.kodein.*
import com.github.salomonbrys.kodein.conf.KodeinGlobalAware
import com.github.salomonbrys.kodein.conf.global
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import io.realm.Realm
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


class App : Application(), KodeinGlobalAware {

    init {
        instance = this
    }


    override fun onCreate() {
        super.onCreate()
        Kodein.global.addConfig {

            bind<RestService>() with singleton {  instance<Retrofit>().create(RestService::class.java) }

            bind<Retrofit>() with singleton { Retrofit.Builder()
                    .baseUrl("http://207.154.248.163:5000")
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .addConverterFactory(MoshiConverterFactory.create())
                    .client(instance())
                    .build() }

            bind<OkHttpClient>() with singleton { OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .build() }

            bind() from singleton { UserService() }

            bind() from singleton { RealmService() }
        }
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        Realm.init(this)
    }

    companion object {
        private var instance : App? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }

        var sharedPreferences : SharedPreferences? = null
            private set
    }
}