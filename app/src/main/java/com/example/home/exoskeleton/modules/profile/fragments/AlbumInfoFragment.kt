package com.example.home.exoskeleton.modules.profile.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.home.exoskeleton.R
import com.example.home.exoskeleton.navigation.MenuItemHolder
import com.example.home.exoskeleton.navigation.NavigationStackPresentable
import com.example.home.exoskeleton.storage.Album
import kotlinx.android.synthetic.main.fragment_album_info.*
import pro.horovodovodo4ka.bones.Phalanx
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.ScreenInterface
import pro.horovodovodo4ka.bones.ui.delegates.Page

class AlbumInfoBone(val album: Album) : Phalanx(), NavigationStackPresentable {
    override val fragmentTitle: String
        get() = "Альбом"
    override var menuItem: MenuItemHolder?
        get() = null
        set(value) {}
    override val seed = { AlbumInfoFragment() }
}

class AlbumInfoFragment : Fragment(), ScreenInterface<AlbumInfoBone> by Page(), BonePersisterInterface<AlbumInfoBone> {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_album_info, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(bone.album){
            albumName.text = title
            albumDescription.text = description
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<android.support.v4.app.Fragment>.onSaveInstanceState(outState)
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<android.support.v4.app.Fragment>.onCreate(savedInstanceState)
    }
}