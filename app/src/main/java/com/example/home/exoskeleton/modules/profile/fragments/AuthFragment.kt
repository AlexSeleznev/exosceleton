package com.example.home.exoskeleton.modules.profile.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.home.exoskeleton.R
import kotlinx.android.synthetic.main.fragment_auth.*
import kotlinx.android.synthetic.main.fragment_sign_up.*
import pro.horovodovodo4ka.bones.Bone
import pro.horovodovodo4ka.bones.BoneSibling
import pro.horovodovodo4ka.bones.Phalanx
import pro.horovodovodo4ka.bones.extensions.present
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.ScreenInterface
import pro.horovodovodo4ka.bones.ui.delegates.Page

class AuthBone : Phalanx() {
    fun showSignInDialog() {
        present(SignUpBone())
    }

    fun showSignUpDialog() {
        present(SignInBone())
    }

    override val seed = { AuthFragment() }
}

class AuthFragment : Fragment(), ScreenInterface<AuthBone> by Page(), BonePersisterInterface<AuthBone> {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_auth, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        signUpBtn.setOnClickListener {
            bone.showSignInDialog()
        }

        signInBtn.setOnClickListener {
            bone.showSignUpDialog()
        }
    }


    //==================Persistance================

    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<android.support.v4.app.Fragment>.onSaveInstanceState(outState)
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<android.support.v4.app.Fragment>.onCreate(savedInstanceState)
    }

    //=====================

}