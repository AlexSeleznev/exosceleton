package com.example.home.exoskeleton.modules.profile.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.home.exoskeleton.R
import com.example.home.exoskeleton.modules.profile.AlbumDataSource
import com.example.home.exoskeleton.modules.profile.adapter.ProfileAlbumAdapter
import com.example.home.exoskeleton.navigation.MenuItemHolder
import com.example.home.exoskeleton.navigation.NavigationStackBone
import com.example.home.exoskeleton.navigation.NavigationStackPresentable
import com.example.home.exoskeleton.services.RealmService
import com.example.home.exoskeleton.services.UserService
import com.example.home.exoskeleton.storage.Album
import com.example.home.exoskeleton.storage.User
import com.github.salomonbrys.kodein.conf.KodeinGlobalAware
import com.github.salomonbrys.kodein.instance
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*
import pro.horovodovodo4ka.bones.Bone
import pro.horovodovodo4ka.bones.Phalanx
import pro.horovodovodo4ka.bones.extensions.closest
import pro.horovodovodo4ka.bones.extensions.present
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.ScreenInterface
import pro.horovodovodo4ka.bones.ui.delegates.Page

class ProfileBone : Phalanx(), NavigationStackPresentable, KodeinGlobalAware {

    private val realmService: RealmService = instance()
    private val userService: UserService = instance()

    val user: User?
        get() = realmService.getUserInfo()

    override val fragmentTitle: String
        get() = "Профиль"
    override var menuItem: MenuItemHolder?
        get() = MenuItemHolder().apply {
            menuRes = R.menu.profile_toolbar_menu
            menuItemClickListener = { item ->
                when (item.itemId) {

                    R.id.toolbar_add_album -> {
                        val dlg = AddAlbumBone()
                        addAndSubscribe(dlg)
                        present(dlg)
                    }
                    R.id.toolbar_change_avatar -> {

                    }
                    R.id.toolbar_exit -> {
                        userService.logOut()
                    }

                }
                true
            }
        }
        set(value) {}

    override val seed = { ProfileFragment() }

    override fun onBoneChanged(bone: Bone) {
        when (bone) {
            is AddAlbumBone -> {
                notifyChange()
            }
        }
    }

    fun showAlbumInfo(it: Album) {
        closest<NavigationStackBone>()?.push(AlbumInfoBone(it))
    }
}

class ProfileFragment : Fragment(), ScreenInterface<ProfileBone> by Page(), BonePersisterInterface<ProfileBone> {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(bone.user) {
            userName.text = "${this?.name}/${this?.login}"
            albumCount.text = this?.albumCount.toString()
            cardCount.text = this?.photocardCount.toString()
            Picasso.with(context).load(this?.avatar).into(userAvatar)
        }
        refreshAdapter()
    }

    override fun onBoneChanged() {
        if (!isResumed) return
        if (bone.user != null) refreshAdapter()
    }

    private fun refreshAdapter() {
        with(userAlbumRecycler) {
            layoutManager = GridLayoutManager(context, 2)
            adapter = ProfileAlbumAdapter(AlbumDataSource()).also {
                it.listener = {
                    bone.showAlbumInfo(it)
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<android.support.v4.app.Fragment>.onSaveInstanceState(outState)
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<android.support.v4.app.Fragment>.onCreate(savedInstanceState)
    }
}