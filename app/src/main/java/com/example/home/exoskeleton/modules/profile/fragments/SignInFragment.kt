package com.example.home.exoskeleton.modules.profile.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.home.exoskeleton.App
import com.example.home.exoskeleton.R
import com.example.home.exoskeleton.network.req.UserSignInReq
import com.example.home.exoskeleton.services.UserService
import com.github.salomonbrys.kodein.conf.KodeinGlobalAware
import com.github.salomonbrys.kodein.instance
import kotlinx.android.synthetic.main.fragmrnt_sign_in.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import pro.horovodovodo4ka.bones.Phalanx
import pro.horovodovodo4ka.bones.extensions.dismiss
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.ScreenInterface
import pro.horovodovodo4ka.bones.ui.delegates.Page
import pro.horovodovodo4ka.bones.ui.helpers.BoneDialogFragment

class SignInBone : Phalanx(), KodeinGlobalAware {
    private val userService: UserService = instance()

    fun signInUser(request: UserSignInReq) {
        launch(UI) {
            try {
                userService.signIn(request)
                Toast.makeText(App.applicationContext(), "Success", Toast.LENGTH_LONG).show()
            } catch (e: Exception) {
                Toast.makeText(App.applicationContext(), e.message, Toast.LENGTH_LONG).show()
            } finally {
                dismiss()
            }

        }
    }

    override val seed = { SignInFragment() }
}

class SignInFragment : BoneDialogFragment<SignInBone>(),
    ScreenInterface<SignInBone> by Page(),
    BonePersisterInterface<SignInBone> {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragmrnt_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sign_btn.setOnClickListener {
            val mail = email_et.text.toString()
            val password = password_et.text.toString()

            val request = UserSignInReq(mail, password)
            bone.signInUser(request)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<BoneDialogFragment>.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<BoneDialogFragment>.onCreate(savedInstanceState)
    }
}