package com.example.home.exoskeleton.modules.cardlist.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import pro.horovodovodo4ka.bones.Bone
import pro.horovodovodo4ka.bones.BoneSibling
import pro.horovodovodo4ka.bones.Phalanx
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.ScreenInterface
import pro.horovodovodo4ka.bones.ui.delegates.Page

class SearchBone: Phalanx(){
    override val seed = { SearchFragment()}
}

class SearchFragment: Fragment(), ScreenInterface<SearchBone> by Page(), BonePersisterInterface<SearchBone>{

    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<android.support.v4.app.Fragment>.onSaveInstanceState(outState)
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<android.support.v4.app.Fragment>.onCreate(savedInstanceState)
    }
}