package com.example.home.exoskeleton.modules.profile.fragments

import com.example.home.exoskeleton.navigation.NavigationStackBone
import com.example.home.exoskeleton.services.UserService
import com.example.home.exoskeleton.services.UserServiceDelegate
import com.github.salomonbrys.kodein.conf.KodeinGlobalAware
import com.github.salomonbrys.kodein.instance

class ProfileNavigationStack : NavigationStackBone(), KodeinGlobalAware, UserServiceDelegate {

    private val service: UserService = instance()
    private val userToken = service.getUserToken()

    init {
        service.delegate += this

        when (userToken) {
            null -> {
                push(AuthBone())
            }
            else -> {
                push(ProfileBone())
            }
        }

    }

    override fun userWasChanged() {
        val userToken = service.getUserToken()
        when (userToken) {
            null -> {
                replace(rootPhalanx, with = AuthBone())
            }
            else -> {
                replace(rootPhalanx, with = ProfileBone())
            }
        }

    }

}