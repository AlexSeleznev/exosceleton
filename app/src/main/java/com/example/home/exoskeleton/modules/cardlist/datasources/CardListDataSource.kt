package com.example.home.exoskeleton.modules.cardlist.datasources

import com.example.home.exoskeleton.services.RealmService
import com.example.home.exoskeleton.storage.PhotoCard
import com.github.salomonbrys.kodein.conf.KodeinGlobalAware
import com.github.salomonbrys.kodein.instance


class CardListDataSource : KodeinGlobalAware {
    private val realmService: RealmService = instance()

    val cardList: List<PhotoCard>
        get() {
            return realmService.cardList.sortedByDescending { it.views }
        }
}