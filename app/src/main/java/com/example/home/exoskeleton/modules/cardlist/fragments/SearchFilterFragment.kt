package com.example.home.exoskeleton.modules.cardlist.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.home.exoskeleton.R
import com.example.home.exoskeleton.navigation.MenuItemHolder
import com.example.home.exoskeleton.navigation.NavigationStackPresentable
import kotlinx.android.synthetic.main.fragment_search_filter.*
import pro.horovodovodo4ka.bones.Phalanx
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.ScreenInterface
import pro.horovodovodo4ka.bones.ui.delegates.Page
import pro.horovodovodo4ka.bones.ui.helpers.BonesFragmentPagerAdapter

class SearchFilterBone : Phalanx(), NavigationStackPresentable {
    override val fragmentTitle: String
        get() = "Поиск"
    override var menuItem: MenuItemHolder?
        get() = null
        set(value) {}
    override val seed = { SearchFilterFragment() }

    val tabs = listOf(
            "Поиск" to SearchBone().also { add(it) },
            "Фильтры" to FiltersBone().also { add(it) }
    )
}

class SearchFilterFragment : Fragment(),
        ScreenInterface<SearchFilterBone> by Page(),
        BonePersisterInterface<SearchFilterBone> {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_search_filter, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchFilterViewPager.offscreenPageLimit = bone.tabs.size
        searchFilterViewPager.adapter = BonesFragmentPagerAdapter(childFragmentManager, bone.tabs)
        searchFilterTabLayout.setupWithViewPager(searchFilterViewPager)
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<Fragment>.onSaveInstanceState(outState)
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<Fragment>.onCreate(savedInstanceState)
    }
}