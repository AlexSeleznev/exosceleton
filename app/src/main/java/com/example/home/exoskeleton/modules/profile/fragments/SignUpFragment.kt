package com.example.home.exoskeleton.modules.profile.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.home.exoskeleton.App
import com.example.home.exoskeleton.R
import com.example.home.exoskeleton.network.req.UserSignUpReq
import com.example.home.exoskeleton.services.UserService
import com.github.salomonbrys.kodein.conf.KodeinGlobalAware
import com.github.salomonbrys.kodein.instance
import kotlinx.android.synthetic.main.fragment_sign_up.*
import kotlinx.coroutines.experimental.runBlocking
import pro.horovodovodo4ka.bones.Phalanx
import pro.horovodovodo4ka.bones.extensions.dismiss
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.ScreenInterface
import pro.horovodovodo4ka.bones.ui.delegates.Page
import pro.horovodovodo4ka.bones.ui.helpers.BoneDialogFragment

class SignUpBone : Phalanx(), KodeinGlobalAware {

    private val userService: UserService = instance()

    fun userSignUp(userSignUpReq: UserSignUpReq) {
        runBlocking {
            try {
                userService.signUp(userSignUpReq)
                Toast.makeText(App.applicationContext(), "Новый пользователь успешно создан", Toast.LENGTH_LONG).show()
            }
            catch (e: Exception){
                 Toast.makeText(App.applicationContext(), "Данные заполнены неправильно", Toast.LENGTH_LONG).show()
            }finally {
                dismiss()
            }

        }
    }

    override val seed = { SignUpFragment() }
}


class SignUpFragment : BoneDialogFragment<SignUpBone>(), ScreenInterface<SignUpBone> by Page(), BonePersisterInterface<SignUpBone> {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_sign_up, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        signUpbtn.setOnClickListener {
            val name = registrationName.text.toString()
            val login = registrationLogin.text.toString()
            val password = registrationPassword.text.toString()
            val mail = registrationMail.text.toString()

            val userSignUpReq = UserSignUpReq(name, login, mail, password)

            bone.userSignUp(userSignUpReq)
        }

        cancelBtn.setOnClickListener {
            dismiss()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<BoneDialogFragment>.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<BoneDialogFragment>.onCreate(savedInstanceState)
    }
}