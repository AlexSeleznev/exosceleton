package com.example.home.exoskeleton.navigation

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.example.home.exoskeleton.R
import kotlinx.android.synthetic.main.fragment_navigation_stack.*
import pro.horovodovodo4ka.bones.Bone
import pro.horovodovodo4ka.bones.Finger
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.FingerNavigatorInterface
import pro.horovodovodo4ka.bones.ui.delegates.FingerNavigator
import pro.horovodovodo4ka.bones.ui.extensions.addNavigationToToolbar
import pro.horovodovodo4ka.bones.ui.extensions.removeNavigationFromToolbar

interface NavigationStackPresentable {
    val fragmentTitle: String
    var menuItem : MenuItemHolder?
}

open class NavigationStackBone(rootPhalanx : Bone? = null) : Finger(rootPhalanx) {
    override val seed = { NavigationStackFragment()}

    fun updateMenu(){
        notifyChange()
    }
}

class NavigationStackFragment : Fragment(), BonePersisterInterface<NavigationStackBone>,
        FingerNavigatorInterface<NavigationStackBone> by FingerNavigator(R.id.stack_fragment_container){

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        managerProvider = ::getChildFragmentManager
    }

    override fun onDetach() {
        super.onDetach()
        managerProvider = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        refreshUI()
    }

    override fun onBoneChanged() {
        if(!isResumed) return
        refreshUI()
    }

    @SuppressLint("MissingSuperCall")
    override fun onRefresh() {
        super<FingerNavigatorInterface>.onRefresh()
        if (view == null) return

        val title = (bone.fingertip as? NavigationStackPresentable)?.fragmentTitle
        when (title) {
            null -> navigationToolbar.visibility = View.GONE
            else -> {
                navigationToolbar.visibility = View.VISIBLE
                navigationToolbar.title = title

                if (bone.phalanxes.size > 1) addNavigationToToolbar(navigationToolbar, R.drawable.ic_arrow_back_white)
                else removeNavigationFromToolbar(navigationToolbar)
            }
        }
        val menu = (bone.fingertip as? NavigationStackPresentable)?.menuItem
        if(navigationToolbar.menu!=null){
            navigationToolbar.menu.clear()
            menu?.menuRes?.let { navigationToolbar.inflateMenu(it) }
            navigationToolbar.setOnMenuItemClickListener(menu?.menuItemClickListener)
        }else{
            navigationToolbar.menu?.clear()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_navigation_stack, container, false)

    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<Fragment>.onSaveInstanceState(outState)
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<Fragment>.onCreate(savedInstanceState)
    }
}

class MenuItemHolder {
    var menuRes : Int? = null
    var menuItemClickListener : ((item : MenuItem )->Boolean)? = null
}