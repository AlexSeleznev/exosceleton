package com.example.home.exoskeleton.network.req

data class UserSignUpReq(
        val name : String,
        val login : String,
        val email : String,
        val password : String
)