package com.example.home.exoskeleton.modules.profile.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.home.exoskeleton.R
import com.example.home.exoskeleton.network.req.AddAlbumReq
import com.example.home.exoskeleton.services.UserService
import com.github.salomonbrys.kodein.conf.KodeinGlobalAware
import com.github.salomonbrys.kodein.instance
import kotlinx.android.synthetic.main.dialog_add_album.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import pro.horovodovodo4ka.bones.Phalanx
import pro.horovodovodo4ka.bones.extensions.dismiss
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.ScreenInterface
import pro.horovodovodo4ka.bones.ui.delegates.Page
import pro.horovodovodo4ka.bones.ui.helpers.BoneDialogFragment

class AddAlbumBone : Phalanx(), KodeinGlobalAware {
    private val userService: UserService = instance()
    fun addAlbum(request: AddAlbumReq) {
        launch(UI) {
            userService.addUserAlbum(request)
            notifyChange()
            dismiss()
        }
    }

    override val seed = { AddAlbumFragment() }
}

class AddAlbumFragment : BoneDialogFragment<AddAlbumBone>(), ScreenInterface<AddAlbumBone> by Page(),
    BonePersisterInterface<AddAlbumBone> {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.dialog_add_album, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        apply_btn.setOnClickListener {
            val albumName = albumName.text.toString()
            val albumDesc = albumDescription.text.toString()
            val request = AddAlbumReq(albumName, albumDesc)
            bone.addAlbum(request)
        }
        cancel_btn.setOnClickListener { bone.dismiss() }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<BoneDialogFragment>.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<BoneDialogFragment>.onCreate(savedInstanceState)
    }
}