package com.example.home.exoskeleton.network.res


data class PhotoCardRes(val id: String,
                        val owner: String,
                        val title: String,
                        val photo: String,
                        val views: Int,
                        val favorite: Int,
                        val tags: List<String>,
                        val filters: Filters?
)

data class Filters(val dish: String,
                   val nuances: String,
                   val decor: String,
                   val temperature: String,
                   val light: String,
                   val lightDirection: String,
                   val lightSource: String
)