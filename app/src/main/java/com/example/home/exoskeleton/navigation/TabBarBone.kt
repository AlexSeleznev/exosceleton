package com.example.home.exoskeleton.navigation

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.home.exoskeleton.R
import kotlinx.android.synthetic.main.activity_base.*
import pro.horovodovodo4ka.bones.Bone
import pro.horovodovodo4ka.bones.Finger
import pro.horovodovodo4ka.bones.Wrist
import pro.horovodovodo4ka.bones.persistance.BonePersisterInterface
import pro.horovodovodo4ka.bones.ui.WristNavigatorInterface
import pro.horovodovodo4ka.bones.ui.delegates.WristNavigator
import pro.horovodovodo4ka.bones.ui.extensions.indexOf


class TabBarBone(vararg bone: Bone) : Wrist(*bone) {
    override val seed = { TabBarFragment() }
}

class TabBarFragment : Fragment(),
        BonePersisterInterface<TabBarBone>,
        WristNavigatorInterface<TabBarBone> by WristNavigator(R.id.content_container_base_activity, true) {

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        managerProvider = ::getChildFragmentManager
    }

    override fun onDetach() {
        super.onDetach()
        managerProvider = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.activity_base, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(bottomBar) {
            selectedItemId = menu.getItem(bone.activeBoneIndex).itemId

            setOnNavigationItemSelectedListener { item ->
                val oldIdx = bone.activeBoneIndex

                bone.activeBoneIndex = menu.indexOf(item)

                if (oldIdx == bone.activeBoneIndex) (bone.activeBone as? Finger)?.popToRoot()
                true
            }
        }

        refreshUI()
    }

    @SuppressLint("MissingSuperCall")
    override fun onRefresh() {
        super<WristNavigatorInterface>.onRefresh()

        with(bottomBar) {
            val itemId = menu.getItem(bone.activeBoneIndex).itemId

            if (selectedItemId == itemId) return

            selectedItemId = itemId
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super<BonePersisterInterface>.onSaveInstanceState(outState)
        super<Fragment>.onSaveInstanceState(outState)
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super<BonePersisterInterface>.onCreate(savedInstanceState)
        super<Fragment>.onCreate(savedInstanceState)
    }
}