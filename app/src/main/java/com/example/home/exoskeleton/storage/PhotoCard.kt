package com.example.home.exoskeleton.storage

import com.example.home.exoskeleton.network.res.PhotoCardRes
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class PhotoCard() : RealmObject() {
    @PrimaryKey
    var id = ""
    var owner = ""
    var title = ""
    var photo = ""
    var views = 0
    var favorites = 0
    var tags = RealmList<String>()
    var dish: String? = ""
    var nuances: String? = ""
    var decor: String? = ""
    var temperature: String? = ""
    var light: String? = ""
    var lightDirection: String? = ""
    var lightSource: String? = ""

    constructor(userCards: PhotoCardRes) : this() {
        this.id = userCards.id
        this.owner = userCards.owner
        this.title = userCards.title
        this.photo = userCards.photo
        this.views = userCards.views
        this.favorites = userCards.favorite
        this.tags = RealmList(*userCards.tags.toTypedArray())
        this.dish = userCards.filters?.dish
        this.nuances = userCards.filters?.nuances
        this.decor = userCards.filters?.decor
        this.temperature = userCards.filters?.temperature
        this.light = userCards.filters?.light
        this.lightDirection = userCards.filters?.lightDirection
        this.lightSource = userCards.filters?.lightSource
    }
}