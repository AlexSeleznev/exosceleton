package com.example.home.exoskeleton.services

import com.example.home.exoskeleton.App
import com.example.home.exoskeleton.network.RestService
import com.example.home.exoskeleton.network.req.AddAlbumReq
import com.example.home.exoskeleton.network.req.UserSignInReq
import com.example.home.exoskeleton.network.req.UserSignUpReq
import com.github.salomonbrys.kodein.conf.KodeinGlobalAware
import com.github.salomonbrys.kodein.instance

class UserService : KodeinGlobalAware {

    private val sharedPreferences = App.sharedPreferences
    private val restService : RestService = instance()
    private val realmService : RealmService = instance()
    var delegate : List<UserServiceDelegate> = emptyList()

    suspend fun signUp(userSigUpReq : UserSignUpReq){
        val user  = restService.sigUpUser(userSigUpReq).await()
        val token = user.token
        realmService.saveUserInfo(user)
        saveUserToken(token)
    }

    suspend fun signIn(userSignInReq: UserSignInReq){
        val response = restService.signInUser(userSignInReq).await()
        when(response.code()){
            200->{
                val body = response.body()
                val token = body?.token
                realmService.saveUserInfo(body!!)
                saveUserToken(token ?: "")
            }
            404->{
                throw Exception("Пользователь не существует")
            }
        }
    }

    fun logOut(){
        saveUserToken(null)
        realmService.logout()
    }

    private fun saveUserToken(token : String?){
        val editor = sharedPreferences?.edit()
        editor?.putString("USER_TOKEN", token)
        editor?.apply()
        for(d in delegate){
            d.userWasChanged()
        }
    }

    fun getUserToken():String?{
        return sharedPreferences?.getString("USER_TOKEN", null)
    }

   suspend fun addUserAlbum(request : AddAlbumReq){
        val userID = realmService.getUserInfo()?.id
        val albumResponse = restService.createAlbum(userID!!, getUserToken()!!, request).await()
        realmService.saveAlbumToRealm(albumResponse, userID, getUserToken())
    }

}

interface UserServiceDelegate{
    fun userWasChanged()
}