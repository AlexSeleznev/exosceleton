package com.example.home.exoskeleton.storage

import com.example.home.exoskeleton.network.res.UserRes
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class User() : RealmObject() {

    @PrimaryKey
    var id: String = ""
    var name: String = ""
    var login: String = ""
    var albums: RealmList<Album> = RealmList()
    var token: String = ""
    var avatar: String = ""
    var photocardCount: Int = 0
    var albumCount: Int = 0

    constructor(user: UserRes) : this() {
        this.id = user.id
        this.name = user.name
        this.login = user.login
        this.albums = RealmList(*user.albums.map { Album(it) }.toTypedArray())
        this.token = user.token
        this.avatar = user.avatar
        this.photocardCount = user.photocardCount
        this.albumCount = user.albumCount
    }
}